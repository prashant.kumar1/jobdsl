job('aws-instance-state')
{
 
  parameters
  {
     stringParam("instance","i-06031121f224427b8","Enter instance id ")
     choiceParam("state",["start","stop"])

    
  }
  steps
  {
     shell(
      '''
         if [ ${state} = "start" ]
         then
            aws ec2 start-instances --instance-ids ${instance} ;
            echo "AWS started"
         fi
      
         if [ "${state}" = "stop" ];then
            aws ec2 stop-instances --instance-ids ${instance} ;
            echo "AWS stopped"
         fi
         
      '''
      )
     
    
  }
  
  
}